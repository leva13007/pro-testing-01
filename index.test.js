const { sum, subtract } = require("./math");

test("sum adds numbers",  () => {
  let result = sum(2,5);
  let expected = 7;

  expect(result).toBe(expected);
});

test("subtract subtracts numbers", () => {
  let result = subtract(2,5);
  let expected = -3;

  expect(result).toBe(expected);
});
